from __future__ import print_function
import boto3
from boto3.dynamodb.conditions import Key, Attr
from botocore.exceptions import ClientError
from datetime import date
import datetime
import random

dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('storagedb')

# --------------- Helpers that build all of the responses ----------------------

def build_speechlet_response(title, output, reprompt_text, should_end_session):
    return {
        'outputSpeech': {
            'type': 'PlainText',
            'text': output
        },
        'card': {
            'type': 'Simple',
            'title': "SessionSpeechlet - " + title,
            'content': "SessionSpeechlet - " + output
        },
        'reprompt': {
            'outputSpeech': {
                'type': 'PlainText',
                'text': reprompt_text
            }
        },
        'shouldEndSession': should_end_session
    }


def build_response(session_attributes, speechlet_response):
    return {
        'version': '1.0',
        'sessionAttributes': session_attributes,
        'response': speechlet_response
    }


# --------------- Functions that control the skill's behavior ------------------

def get_welcome_response():
    """ If we wanted to initialize the session to have some attributes we could
    add those here
    """

    session_attributes = {}
    card_title = "Welcome"
    speech_output = "Please specify student name" 
    reprompt_text = "Please, ask me anything else" 
    should_end_session = False
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))
        
def on_launch(launch_request, session):
    """ Called when the user launches the skill without specifying what they
    want
    """

    print("on_launch requestId=" + launch_request['requestId'] +
          ", sessionId=" + session['sessionId'])
    userID = session['user']['userId']

    # Dispatch to your skill's launch
    return get_welcome_response()
    
def handle_session_end_request():
    card_title = "Session Ended"
    speech_output = "Good bye!"
    should_end_session = True
    return build_response({}, build_speechlet_response(
        card_title, speech_output, None, should_end_session))

def retrieve_on_intent1(username, flag):
    course = " "
    ID = " "
    attendance = " "
    percentage = " "
    certifications = " "
    year = " "
    try:
        response = table.query(
        KeyConditionExpression=Key('username').eq(username)
        )
        for item in enumerate(response['Items']):
            course = item[1]["course"]
            ID = item[1]['ID']
            attendance = item[1]['attendance']
            year = item[1]['year']
            percentage = item[1]['percentage']
            certifications = item[1]['certifications']
    except ClientError as e:
        return e.response['Error']['Code']
    if flag == 1:
        return percentage
    elif flag == 2:
        return certifications
    elif flag == 0:
        return " is a " + year + " student bearing ID " + ID + " enrolled into " + course + " course. "
    elif flag == 3:
        return attendance
    else:
        return 0
    
def on_intent(intent_request, session):
    """ Called when the user specifies an intent for this skill """

    print("on_intent requestId=" + intent_request['requestId'] +
          ", sessionId=" + session['sessionId'])

    intent = intent_request['intent']
    intent_name = intent_request['intent']['name']
    userID = session['user']['userId']
    if intent_name == "StudentDetails":
        return set_username_in_session(intent, session)
    elif intent_name == "PercentageAcademics":
        return get_username_from_session(intent, session)
    elif intent_name == "CertifiedAt":
        return get_skill2_from_session(intent, session)
    elif intent_name == "AttendancePercentage":
        return get_attendance_from_session(intent, session)
    elif intent_name == "AMAZON.CancelIntent" or intent_name == "AMAZON.StopIntent":
        return handle_session_end_request()
    elif intent_name == "AMAZON.HelpIntent":
        return get_welcome_response()
    else:
         raise ValueError("Invalid intent")    
         
def set_username_in_session(intent, session):
    card_title = intent['name']
    session_attributes = {}
    should_end_session = False

    if 'username' in intent['slots']:
        username = intent['slots']['username']['value']
        session_attributes = create_username_attributes(username)
        output1 = retrieve_on_intent1(username, flag = 0)
        speech_output = username + output1
        reprompt_text = "Go ahead and ask me more about " + username
    else:
        speech_output = "I'm not sure what you said." \
                        "Please try again."
        reprompt_text = "I'm not sure what you said. " \
                        "You can ask me by saying, " \
                        "student details of <student name>."
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def create_username_attributes(username):
    return {"username": username}

def get_username_from_session(intent, session):
    session_attributes = {}
    reprompt_text = None
    if "username" in session.get('attributes', {}):
        username = session['attributes']['username']
        session_attributes = create_username_attributes(username)
        speech_output = username + " has an academic percentage of " + retrieve_on_intent1(username, flag = 1)
        should_end_session = False
    else:
        speech_output = "I'm not sure what you said " 
        should_end_session = False
    return build_response(session_attributes, build_speechlet_response(
        intent['name'], speech_output, reprompt_text, should_end_session))


def get_attendance_from_session(intent, session):
    session_attributes = {}
    reprompt_text = None
    #characteristics = ['Adventurous', 'Generous', 'Active', 'Strange', 'Caring', 'Humble', 'Daring', 'Original', 'Brave', 'Self-assured', 'Interesting']
    if "username" in session.get('attributes', {}):
        username = session['attributes']['username']
        session_attributes = create_username_attributes(username)
        #speech_output = username + " is a very " + random.sample(set(characteristics), 3)[0] + ', ' + random.sample(set(characteristics), 3)[1] + ', ' + random.sample(set(characteristics), 3)[2]+ ' person'
        speech_output = username + " has an attendance percentage of " + retrieve_on_intent1(username, flag = 3)
        should_end_session = False
    else:
        speech_output = "I'm not sure what you said " 
        should_end_session = False
    return build_response(session_attributes, build_speechlet_response(
        intent['name'], speech_output, reprompt_text, should_end_session))

    
def get_skill2_from_session(intent, session):
    session_attributes = {}
    reprompt_text = None
    #characteristics = ['Adventurous', 'Generous', 'Active', 'Strange', 'Caring', 'Humble', 'Daring', 'Original', 'Brave', 'Self-assured', 'Interesting']
    if "username" in session.get('attributes', {}):
        username = session['attributes']['username']
        session_attributes = create_username_attributes(username)
        #speech_output = username + " is a very " + random.sample(set(characteristics), 3)[0] + ', ' + random.sample(set(characteristics), 3)[1] + ', ' + random.sample(set(characteristics), 3)[2]+ ' person'
        speech_output = username + " is certified on " + retrieve_on_intent1(username, flag = 2)
        should_end_session = False
    else:
        speech_output = "I'm not sure what you said " 
        should_end_session = False
    return build_response(session_attributes, build_speechlet_response(
        intent['name'], speech_output, reprompt_text, should_end_session))


def lambda_handler(event, context):
    print("event.session.application.applicationId=" +
          event['session']['application']['applicationId'])
    try:
        if (event['session']['application']['applicationId'] != "amzn1.ask.skill.28295548-6b37-4153-9a38-ccf9b1ed5bff"):
            raise ValueError("Invalid Application ID")
        
        if event['request']['type'] == "LaunchRequest":
            return on_launch(event['request'], event['session'])
        elif event['request']['type'] == "IntentRequest":
            return on_intent(event['request'], event['session'])
        
    except ClientError as e:
        print(e.response)
        return e.response['Error']
   