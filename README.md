**Alexa Skills Kit SDK Sample - Student Details**
---
**A simple AWS Lambda function and the skill interaction schema that demonstrates how to write an student details skill for the Amazon Echo using the Alexa Skill Kit Python SDK.**
---
**NOTE:** This sample is subject to change during the beta period.

**Concepts**
---
This sample shows how to create a Lambda function for handling Alexa Skill requests that:


- Demonstrates using custom slot types.
- Use session attributes to store and pass session level attributes. In this example, user name is stored and passed on the skill session.
- Use a catch-all exception handler, for catching all exceptions during skill invocation and return a meaningful response to the user.



**Setup**
---
To run this example skill you need to do two things. The first is to deploy the example code in lambda, and the second is to configure the Alexa skill to use Lambda.


**Alexa Skill Setup**
---
First, navigate to the Alexa Skills Kit Developer Console. Click the "Create Skill" button in the upper right. Enter "EmployeeDetails" as your skill name. On the next page, select "Custom" and click "Create skill".

Now we are ready to define the interaction model for the skill. Under "Invocation" tab on the left side, define your Skill Invocation Name to be employee details.

Now it is time to add an intent to the skill. Click the "Add" button under the Intents section of the Interaction Model. Leave "Create custom intent" selected, enter "EmployeeDetails" for the intent name, and create the intent. Now it is time to add some sample utterances that will be used to invoke the intent. For this example, we have provided the following sample utterances:

```
                        StudentDetails of {userName}
                        fetch student details of {userName}
                        student details of {userName}
                        {userName}
```
`{userName}` is a slot we are using. Slots can found below Built-In Intents. To create one, click "Add Slot Type" and under "Use an existing slot type from built-in library of Alexa" search for "Person" and add "AMAZON.Person" slot type.

Let us add another intent to the skill that has slots, called "AcademicPercentage" for intent name. Skip the sample utterances part for now and create a new slot called "pronoun" Select Slot Type to be "AMAZON.Person". Now add below sample utterances that uses this slot "pronoun".

```
what is {pronoun} academic percentage
{pronoun} academic percentage

```
Since **AMAZON.CancelIntent**, **AMAZON.HelpIntent**, and **AMAZON.StopIntent** are built-in Alexa intents, sample utterances do not need to be provided as they are automatically inherited.

The Developer Console alternately allows you to edit the entire skill model in JSON format by selecting "JSON Editor" on the navigation bar. For this sample, the interaction schema can be used.

```
{
    "interactionModel": {
        "languageModel": {
            "invocationName": "student details",
            "intents": [
                {
                    "name": "AMAZON.FallbackIntent",
                    "samples": []
                },
                {
                    "name": "AMAZON.CancelIntent",
                    "samples": []
                },
                {
                    "name": "AMAZON.HelpIntent",
                    "samples": []
                },
                {
                    "name": "AMAZON.StopIntent",
                    "samples": []
                },
                {
                    "name": "StudentDetails",
                    "slots": [
                        {
                            "name": "username",
                            "type": "AMAZON.Person"
                        }
                    ],
                    "samples": [
                        "{username}",
                        "student details of {username}",
                        "fetch student details of {username}"
                    ]
                },
                {
                    "name": "PercentageAcademics",
                    "slots": [
                        {
                            "name": "pronoun",
                            "type": "AMAZON.Person"
                        }
                    ],
                    "samples": [
                        "what is {pronoun} academic percentage",
                        "{pronoun} academic percentage"
                    ]
                },
                {
                    "name": "CertifiedAt",
                    "slots": [
                        {
                            "name": "pronoun",
                            "type": "AMAZON.Person"
                        }
                    ],
                    "samples": [
                        "what is {pronoun} certified at",
                        "{pronoun} is certified at"
                    ]
                },
                {
                    "name": "AttendancePercentage",
                    "slots": [
                        {
                            "name": "pronoun",
                            "type": "AMAZON.Person"
                        }
                    ],
                    "samples": [
                        "what is {pronoun} attendance percentage",
                        "{pronoun} attendance percentage"
                    ]
                }
            ],
            "types": []
        }
    }
}

```
Once you’re done editing the interaction model don’t forget to save and build the model.

Finally you are ready to test the skill! In the "Test" tab of the developer console you can simulate requests, in text and voice form to your skill. Use the invocation name along with one of the sample utterances we just configured as a guide. You should also be able to go to the Echo webpage and see your skill listed under "Your Skills" where you can enable the skill on your account for testing from an Alexa enabled device.

**AWS Lambda Setup**
---
Refer to [Hosting a Custom Skill as an AWS Lambda Function](https://developer.amazon.com/docs/custom-skills/host-a-custom-skill-as-an-aws-lambda-function.html) reference for a walkthrough on creating a AWS Lambda function with the correct role for your skill. When creating the function, select the "Blueprints" option, and select `alexa-skills-kit-color-expert-python` blueprint, which is easy to get started with.

Further process:
- Give a proper function name.
- Choose create a custom role with `lambda_execution_policy` being its 'IAM Role'. Later dynamodb full access permissions can be configured.
- Add **Alexa Skills Kit trigger** to the lambda function, which is necessary to link lambda to your created skill. Enable skill ID Verification and Specify your skill ID, scroll down to the bottom and hit create function.
- Repalce the blueprint function
- Copy the ARN value specified at the top of the lambda console and paste in the endpoints section of skill created.

**DynamoDB**
---
This skill uses dynamoDB for the storage space. For, lambda to interact with DB configure the role created in the process of initializing the lambda function.

  Process:
- Click on role (Services -> IAM Role)
- Add policies -> DynamoDbFullAccess.   
Now, our lambda is interactive towards the Database.

**Additional Resources**
----
**Community**
- [Amazon Developer Forums](Amazon Developer Forums : Join the conversation!) : Join the conversation!
- [Hackster.io](Hackster.io - See what others are building with Alexa.) - See what others are building with Alexa.

**Tutorials & Guides**
- [Voice Design Guide](https://developer.amazon.com/designing-for-voice/) - A great resource for learning conversational and voice user interface design.

**Documentation**

- [Official Alexa Skills Kit Python SDK Docs](https://github.com/alexa-labs/alexa-skills-kit-sdk-for-python/blob/master/README.rst)
- [Official Alexa Skills Kit Docs](https://developer.amazon.com/docs/ask-overviews/build-skills-with-the-alexa-skills-kit.html)
